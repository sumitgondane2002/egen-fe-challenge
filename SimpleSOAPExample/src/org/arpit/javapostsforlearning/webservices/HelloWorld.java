package org.arpit.javapostsforlearning.webservices;

public class HelloWorld {
	public String sayHelloWorld(String name)  
    {  
        return "Hello world from "+ name;  
    }

//Read more at http://www.java2blog.com/2013/03/soap-web-service-example-in-java-using.html#kYQDUErlPTF8of5s.99
}
