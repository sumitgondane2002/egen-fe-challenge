
package com.sumit.nsm;

import java.util.Scanner;

public class Sumit {
	
	
	public void sum(int a, int b){
		System.out.println(a+b);
	}
	public void sum(int a, int b, int c){
		System.out.println(a+b+c);
	}
	public void sum(float a, float b){
		System.out.println(a+b);
	}

	public static void main(String args[]){
		Sumit sumobj = new Sumit();
		//overloading
		sumobj.sum(10,20);
		sumobj.sum(10,20,30);
		
		//overriding
		sumobj.sum(10,20F);
	}
}
